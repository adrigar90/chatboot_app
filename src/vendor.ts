// Third party libraries
import '@angular/common';
import '@angular/compiler';
import '@angular/core';
import '@angular/http';
import '@angular/router';
import '@angular/platform-browser';
import '@angular/platform-browser-dynamic';

import 'rxjs/Rx';

import 'thin2-config';
import 'thin2-log';
import 'thin2-router';
