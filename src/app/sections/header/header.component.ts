import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Response } from '@angular/http';
import { Observable } from 'rxjs/Observable';


@Component({
    selector: 'header-component',
    templateUrl: './header.component.html'
})
export class HeaderComponent implements OnInit {

	private menu: Array<any>;
	
    constructor(private router: Router
    ) { 
    	this.menu =[
    	{
    		entry: 'INICIO',
            url: 'inicio',
    	},{
    		entry: 'CHATBOT',
            url: 'historico',
    	},{
    		entry: 'WEB',
            url: 'web',
    	},{
    		entry: 'DOMAINS',
            url: 'domains',
    	}]
    }

    ngOnInit() {
    	
    }

    goToinit(){
        this.router.navigate(['']);
    }

}
