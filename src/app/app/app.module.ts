import { APP_BASE_HREF } from '@angular/common';
import { APP_INITIALIZER, NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { HttpModule } from '@angular/http';
import { RouterModule, Routes } from '@angular/router';

import { Thin2Config, ConfigService, ConfigLoader } from 'thin2-config';
import { LoggerModule } from 'thin2-log';
import { Thin2RouterService } from 'thin2-router';

import { LocationLoader } from './app.location';
import { MainComponent } from './../views/main/main.component';

import { HeaderComponent } from './../sections/header/header.component';
import { FooterComponent } from './../sections/footer/footer.component';
import { IntroComponent } from './../views/intro/intro.component';
import { NotFoundComponent} from './../views/notFound/notFound.component';
import { WebComponent} from './../views/web/web.component';
import { DomainsComponent} from './../views/domains/domains.component';
import { ChatbotComponent} from './../views/chatbot/chatbot.component';
import { InitComponent} from './../views/inicio/inicio.component';


const routes: Routes  = [

    {path:'', component:IntroComponent},
    {path:'inicio', component: InitComponent},
    {path: 'web', component: WebComponent},
    {path: 'historico', component: ChatbotComponent},
    {path: 'domains', component: DomainsComponent},
    {path: '**', component: NotFoundComponent}

    ];

@NgModule({
    imports: [
        BrowserModule,
        HttpModule,
        RouterModule.forRoot(routes, { useHash: true }),
        LoggerModule,
        Thin2Config
    ],
    declarations: [
        MainComponent,
        FooterComponent,
        HeaderComponent,
        IntroComponent,
        NotFoundComponent,
        WebComponent,
        DomainsComponent,
        ChatbotComponent,
        InitComponent,
    ],
    providers: [
        {
          provide: APP_BASE_HREF,
          useValue: '/'
        },
        {
            provide: APP_INITIALIZER,
            useFactory: ConfigLoader,
            deps: [ConfigService],
            multi: true
        }
    ],
    bootstrap: [
        MainComponent
    ],
})
export class AppModule { }
